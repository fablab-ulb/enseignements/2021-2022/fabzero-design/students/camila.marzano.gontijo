# 4. Laser Cutting: computer assisted cutting

During the Module 4 we have learned how to do laser cutting. Before passing to the machines, we watched a whole introduction regarding safety rules, materials and other important information. 

For laser cutting, it's the vector design that interests us as the machines cannot identify drawing outlines from a raster or bitmap file. With vector drawing we can enlarge our drawing to infinity without loss of quality. When we talk about computer-assisted cutting, we mean by this the idea that we design a drawing model on the computer that we then transmit to the machine so that it can reproduce it with its laser on the materials of our choice, by cutting or engraving.

But what is the difference between vector and raster images? The raster design is made up of a pixel matrix, basically a kind of "map of colored dots" juxtaposed next to each other and the vector design is made of geometric line.

## Safety first! 

### Materials

Here is the list of what you need to know about materials:

**Recommended materials**

-ideally 3 or 4 mm plywood (be careful, wood smokes a lot when cutting)

-acrylic (often called plexiglass, very easy to cut)

-paper

-cardboard

-some textiles (see explanation below)

**Unsuitable materials**

-MDF (produces very toxic resin when cutting which sticks the machine on top of that and can break it)

-ABS, PS (polystyrene which produces a very harmful smoke and melts easily)

-thick polyethylene PE, PET, PP (the cuts are not made cleanly)

-fiber-based composite materials (glass fiber, carbon fiber, etc. Because dust is very harmful)

-metals (very difficult or often impossible cutting)

**Prohibited materials**

-PVC (produces a toxic smoke which damages the machine, you have to be very careful because the appearance is similar to that of plexiglass)

-copper (because it totally reflects the laser beam on the machine itself, which could destroy it)

-teflon (PTFE: acid smoke, and very harmful)

-vinyl or imitation leather (produces acid fumes, if chlorine is present, which destroys the machine)

-phenolic resin, epoxy (very harmful smoke too)

### Machine buttons

First and most important, you have to make sure you have a perfect understanding of the material you want to cut / engrave so as not to damage the machine or your health. Then make sure to always open the compressed air valve before operating the machine (in which case it will light up an orange light if it is not done), turn on the smoke extractor beforehand and also know where is the emergency button (big red button on which one presses in case of concern for the Lasersaur). Finally, before starting the machine, make sure you know where to find a CO2 fire extinguisher in case of a big problem and keep it nearby. If all this is properly applied, we can start laser cutting. 

## Engraving X cutting

The FabLab has two machines, both of which are available for engraving or cutting. 

**What is the difference?**

Engraving marks and hollows out the material, but not all of its thickness. It's up to us to decide how deep it is by making our own settings. The cut splits the material in half. If you are not sure of the result that you will choose, it is preferable to do a test beforehand on the said material, for this there are already ready-made and downloadable patterns to test the different power and speed settings of the laser (which will influence the thickness of the cutting / engraving).

**Important to know:** when we create our vectorized image, we must put a color per type of line whose settings will be different. It is possible, for example, on the same drawing to do cutting and engraving, but it will then be essential for the machine to know how to distinguish the two and to configure them according to the expected result. So we apply a color to each one as can be seen in the image above, each color will be set according to its power (%) and speed (F) and will allow the machine to know what to do.

![](../images/tests.jpg)

## Fablab Machines

The ULB FabLab has 2 machines: the Lasersaur and the EpilogFusion. They are able to do the same job however they operate differently.

**Laserseur specifications**:

Cutting area: 122 x 61 cm
Maximum height: 12 cm
Maximum speed: 6000 mm / min
LASER power: 100 W
LASER type: CO2 tube (infrared)

It is an open-source machine designed by the FabLab. This machine, by its size, allows you to work on a large surface and make quality cuts thanks to its power. However, it is not recommended to make cuts at full power or speed because the quality will be reduced. The communication interface is the Drive BoardApp. This interface manages SVG or DXF type files. SVG files tend to perform better generally.

![](../images/laserseur.jpg)

**Epilog Fusion specifications**:

Cutting area: 81 x 50 cm
Maximum height: 31 cm
LASER power: 60 W
LASER type: CO2 tube (infrared)

This machine is smaller than the Lasersaur and uses Inkscape as a communication interface.

![](../images/epilog.jpg)

## Testing different speeds and potencies: group exercise

We started with a familiarization with what computer-assisted cutting is and how the machines at the ULB FabLab work.

We have done this exercice in group of 5 people. Together we decided to test the different levels of cutting by varying the speend and the potency each time; this means we tested a speed with all different potencies and the other way around. 

We had decided to make it on Affinity Designer, unfortunately the file was not taken into account by the laser cutter program, we had to start over. It's quite simple, for each given value, you need a given color and these values change on the right. You have to be methodical when you do it because it is easy to make mistakes. Be careful to organize the colors so that the interior shapes are made first.

![](../images/laser.jpg)

For this image I would like to give the credits to Maïlys, that occupied to do the montage of the pictures we have registered together as a group.

That way we understood which configurations to use once we use the machine, to do either cutting, folding or maybe just graphing a certain drawing. We tested in the polypropylene sheet, as it was the one we were supposed to use for the lamp project.  

## The lamp exercise

In order to really know the machines, we were proposed an exercise of creating a light on a polypropylene sheet. The polypropylene is then, cutted out of one of the laser machines. We were also asked for this assignment to be able to create this lamp without use of any glue or attachment then the polypropylene sheet. To do the lasercut it's necessary to chose the right speed and intensity of the laser, that way you won't risk either to burn or to not cut at all your polypropylene. Remember that each material reacts in its own way. The best is always to do tests beforehand.

## Research and personal project

The light we were asked to create, should be something adapted to kids, so I had in mind to create something fun, playful, that could adapt the intensity of the light, regarding the evening rotine of the kid. For example, a stronger light when the kid is reading a book and something more indirect and stuffy when it's time to calm down before sleeping. So I thought to make something that could open and close, in different levels of intensity. In addition to that, polypropylene is a flexible and translucent material, therefore ideal for playing with light and shadow.

My main inspiration was an insect my friends and I used to play with in the garden, the little armadillo. This insect has different body layers pararallel attached to each other. Once you touch it, it closes completely, contracting his layers and becoming a small ball. So, for the lamp I analysed and tried to create someting inspired on the shape of this insect.

![](../images/tatu.jpg)

![](../images/croquis1.jpg)
![](../images/croquis2.jpg)

First, I had an idea to create a rotation axis and I tried many connections between the parts/layers. I had a some failures while doing the prototypes. I will share with you some of my sketches and also the first prototype I did. This first one wasn't the final assemblage, as I changed the way to connect the layer because this one didn't really work the right way: it didn't fix to the light source and it had a weak support to stay still. 

![](../images/testlampe1.jpg)

Then, while testing, things were evoluating and I observed something that was the key for the project: the base of the light. With its fixation part on the top, it meant that I could easily bring the polypropylene layers together and "lock" them with this small part of the base. 

![](../images/testelampe2.jpg)

I did a prototype directly with the polypropylene, cutted manually to test first. That means, I created the final object by testing directly in prototype, as all the systems I have draw before, didn't work. 

![](../images/testelamp1.jpg)
![](../images/testelamp2.jpg)

So once the prototype was done I wrote down all the measurements and did some croquis to explain the model. I have verified and changed a few measurements for a better design look and then I passed to the lasercut machine. Time to laser cut the model!

## Configurations 

Before passing to the machine I did the drawing of the parts I would have to print for my lamp on Autocad. There I did the plan of each part I had to cut on the laser machine, following the exact measurements and also making sure it was on the good scale. 

![](../images/autocad.jpg)

Also, for my 2D model, every line I had to draw, was **red**, which means, they were parts to cut, not to fold or engrave for example. Everything was going to be cutted out with the laser machine.

Once I had the file ready, I had to **export it in .svg** format and drag it onto a USB stick, to be able to connect it with the machine: .svg can be read on **Inkscape**. 

Once the file is opened with Inkscape on the machine's computer, we check that the line thicknesses: to have a fine cut, the **lines must be 1 mm thick** (the thickness of the polypropylene sheet.

Once everything is well setted we can send to the machine to start the job. Making sure that the Epilog is well selected (in my case, because I used the machine Epilog). The machine interface then opens and you can start to configure the assembly. You must first select _pluger_. Then explode the drawing by color layer by clicking on the _color_. After that, make sure to program the right process, here _vector_. To set the power and speed, refer to the calibration test carried out beforehand (the one we did . Here I chose 10% speed for 20% power and adjusting the frequency to 100%, taking about 10 minutes to do the cutting. These parameters were chosen for all the layers. Finally we can start the laser cutting!

![](../images/settings.jpg)

![](../images/epilog1.jpg)

![](../images/epilog2.jpg)

![](../images/epilog3.jpg)

## The final result

I was very happy with the result and to be able to find a solution for the idea that on a first place seemed to be a bit complicated. 

Here are some images of my lamp:

![](../images/final1.jpg)

![](../images/final2.jpg)

![](../images/final3.jpg)

## Another machine, another exercise: Vinyl sticker

![](../images/vinyl2.jpg)

For this exercise I decided to take a Japanese Ramen **.png** simple image to be printed out as a sticker. Remember that for vinyl cutting, it is therefore necessary to be able to vectorize it. I dragged the image into the Silhouette Studio program (program with logo that looks like a butterfly) to do so. 

There are **two colors** to choose for the different functions of the machine: red corresponding to sketch and blue corresponding to cutting. You have to be careful to put the right settings in the right colors because you can quickly get it wrong! The strength will really depend on the material you choose. 

![](../images/vinyl1.jpg)

**Sketch**: force 3, speed 4

**Cutting**: force 7, speed 5

![](../images/vinyl4.jpg)

![](../images/vinyl5.jpg)

When the settings are finished, you can send it to print. Here is the final result, just like I was expecting:

![](../images/vinyl6.jpg)
 
## Useful links

- [Download Inkscape](https://inkscape.org/fr/)
- [Laserseur Guide](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero/machines/-/blob/master/Lasersaur.md)
- [Epilog Guide](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero/machines/-/blob/dc6cd87ffe1610ae9d5b1dd8bab55d222f78ae49/EpilogFusion.md)
- [Laserseur materials](https://github.com/nortd/lasersaur/wiki/materials)
- [Epilog materials](https://www.epiloglaser.com/assets/downloads/fusion-material-settings.pdf)
- [Svg File](../images/lampe.svg)
