# 5. Computer-aided machining: Shaper

## Formation and important information

We did a quite of a short training on the Shaper, a machine similar to a portable mini CNC, which allows to make precision milling on materials not limited by their sizes. You can find the machine, its guide and tools on the official website and have information on how it works on the FabLab website. Check on the tab **'Useful links'** the links that explains better how to use this machine.

![](../images/shaper1.jpg)

![](../images/shaper2.jpg)

## Basic information on the Shaper

Maximum cutting depth: 43 mm
Collar diameter: 8 mm or 1/8"
Imported file format: SVG
Depth: depends on the diameter of the cutter
Abre for the '_fraise_': 6 mm

## Working space 

**Fixation and flat surface:**

For an installation of the work perimeter, it is first necessary to make sure to have a sufficiently clear and stable surface before starting anything. If this is the case, then you must bring your material and fix it perfectly to the work table so that the cut pieces do not "jump" . 

To fix it: if the work surface is stable and smooth, then it is enough to put solid double-sided on all the necessary parts of the board. The ideal is to think about its cutting shape and anticipate it so as to know where to place the tape wisely. If this is not the case, or in doubt, in order to guarantee a good resistance of our materials, we can also screw the board to the worktop.


## How does the Shaper work?

We did not have the time to do an individual exercise this time, but we worked all together in the shaper. We cutted out an illustration form of the Earth. Check the proccess and result below:

![](../images/shaper3.jpg)

![](../images/shaper4.jpg)

![](../images/shaper5.jpg)
 
The shaper works based on a vectorial file, that can be done for example on Inkscape. Import the SVG file imported beforehand with the USB key. Insert the key into the machine, then click on "draw" to view the projection of the drawing on our materials on the screen. When the location of the drawing is good make "place".

To cut into a wooden plate first it is necessary to digitalize the surface.

Then launch the settings (type of cuts: inside, outside, in the middle of the drawing / diameter of the cutter.)

Turn on the milling machine by clicking on the green button and start milling, follow the movements of the arrows indicated.

That's it, the explanations are rather short compared to the other modules but the use is simple. I recommend to follow the recommendations for the implementation and settings, check the links, and then let yourself be guided by the machine. 

For this Module I inspired and would like to give credits on the work of Joana and Clementine, that helped a lot to remind me what we did during the formation.

## Useful links

- [Shaper](https://www.shapertools.com/fr-fr/)
- [Fablab guide on Shaper](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero/machines/-/blob/master/Shaper.md)
- [Guide to use Shaper](https://assets.shapertools.com/manual/Shaper_Origin_Product_Manual.pdf)
