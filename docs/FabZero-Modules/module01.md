# 1. Git Lab 
## Learning how to use Gitlab

This first week we have started the first module. We learned some **coding basics** necessary for using GitLab and making it easier to publish our documents and texts here. The purpose of this platform is to share every process of our personal or collective projects, so we learn the importance of documenting each step we take. That way we can keep a record of what we learn and maybe go back to the information that could be useful for a next project. I will explain and pass by the main steps I took to get you to know how I learned to use this platform.

![](../images/gitphoto.jpg)

## Configurating Gitlab: gitbash and SSH key

To write on this documentary page, some processes were fundamental to get started. It was quite **complex** to understand it, but once you start, you’ll see how **practical** it is.

To begin, I had to **download GIT**, a management software often used to manage documentation. It also allows several people to work on the same project. This is a fast way we can do coding. Then, for things to start working, it was necessary to **install Bash**. The merger of Bash and Git is called Git BASH. 

**What is Git BASH?** It is a quick command system that will take control of the computer and various software, via a terminal. There are various ways to take this step, first of all you should check the options you also can get if you have a Windows 10 or an Apple computer.

Once you have the Git BASH, it will be necessary to configure it by identifying yourself directly via the terminal with your name and email. Make sure to use the same name and e-mail as the one used on GitLab otherwise it will not work.

![](../images/configurations.jpg)

![](../images/step2.JPG)

To connect my computer to GitLab, I had to add security information to authenticate myself, in addition to configuring my credentials via an **SSH key**. Once authenticated, GitLab will no longer ask for credentials. When this key is given, it will be directly saved into your computer, normally in the drive C, in a folder which is titled .ssh. In this folder there is a _private key_ and a _public key_.

![](../images/step1.JPG)

**How too configure GitLab and add the SSH key?** 

Go to GitLab, open the newly created key, copy the key provided and go to the “parameters” of the GitLab personal space to copy it by completing the SSH box. The SSH key is, then, added to GitLab. To verify the identification, you will have to type a code into the terminal.

![](../images/step3key.JPG)

Now after those steps, **you will need to create a remote repository for GitLab**. That is, a specific project in GitLab corresponds to a specific folder on your machine. To do this, you must go to your GitLab account and click on **cloning**. 

**Select the SSH key and copy it to text software.**

![](../images/step5clone.JPG)

![](../images/step4clone.JPG)

Then, choose a folder on the desktop where you want to create the **repository**, open a **Git BASH terminal** in this folder and open the cloned SSH key. 

The file is then created on its own at the desired location, the computer will merge with GitLab as shown on the terminal.

![step6](../images/step6.JPG)

## Writting a text and adding photos

With the previous step, the creation of the repository, **we can now modify everything directly on the desktop without having to go to GitLab**. 

You can add photos, texts, links, etc. It's **simple** and **fast**.

For the texts, simply add and edit them in the repository folder. For special characters on the text, here follows some rules:

Adding ** around the word / phrase makes the selection **bold**

Adding _ around the word / phrase makes the selection _italic_

If you are quoting, put> in front of your sentence so that it is grayed out and there is an offset.

the points are made by the simple use of the dash of 6 cumulated with a space before the text which follows.

For links, it's in parentheses too.

![](../images/linkexample.JPG)

**For the photos**: 

I added them through my repository folder, on my computer. I saved them in the images folder and once I go to the repository folder, I type in the name saved for it in the parenthesis which allows to enter the location and name of storage of the photograph, each space is separated by a slash . Making sure not to forget the .JPG or .jpg: it should be identical to the name of the file in question. The bracketed text is the quick description of the image, but it isn't mandatory to write on it. See example below:

![](../images/step6-step6.JPG)

Be careful, the images are often too large or too bulky. For the reading of our platform to be fluid, it is better to reduce the images, so, compress them. The reduction can be done with Photoshop or other software such as GraphicsMagick (it works with Git BASH). In my case i didn't have any heavy images, they were all making much less than 100KB, so I didn't really have to go through this process yet.

Once it's ready, you'll have to open the file on **ATOM** and stage everything you wish to upload to the website. Then, you can write a message, for example I wrote "Test Git Camila". 

Then, it will be necessary to **Push** and normally you'll have an **update on your page**. Make sure the images and texts are going to appear. Otherwise, you might have to check and repeat the Push.

## Checking updates

To enable and receive GitLab email notifications you must click on your profile, go to "user settings", then look in the column on the left at the "notifications" tab. Once on the notification settings page, you can manage the settings in order to be informed by email. Check below how I did this:

![](../images/notifications.JPG)

The website is updated automatically twice a week overnight. We will check if it worked for the first time tomorrow. We can check if everything went well during these: if the mkdocs file (step 1 of configuration) is misconfigured then the job will show as “failed” in red, otherwise it will be green. After you have done all the steps, you should be able to start using GitLab easily.

## Useful link 

- [Git](https://git-scm.com/)
