## Introducing myself

Hi,
My name is Camila, and I am currently doing a master’s in architecture at ULB La Cambre Horta, in Brussels. For my first year of Master, I have chosen the option of Architecture & Design, because of my motivation to learn how does the process of creating an object or a concept works, as it is something I want to work with in the future of my career. I have chosen to be a part of the Module 3, where we will work at the ADAM Museum with kids, learning how to introduce the design, the materials, the textures to them. I have identified a lot with this project, because of my passion for the pieces in exhibition at this museum and because kids inspire me to have freedom towards a creation process. Looking forward for the next weeks!

![](images/profilepicture.jpg)

## My background

I’ve started my architecture studies in my home country, Brazil, where I have lived in the city of Belo Horizonte until the year of 2018, when I came to Belgium for an Erasmus in Interior Design in a Flemish Art School, LUCA. Even if I have shown interest for creativity subjects in general since I was a little kid, it was during the year of my exchange program that I started to discover better my real interests in the subject of Architecture and my passion for Vintage furniture and design and Contemporary Art. I have also developed a bigger interest to interiors planning, space design and smaller scale projects. As I loved Belgium more and more during my stay, I have taken the decision to stay and to do a transference of my studies from Brazil to here. I have learnt a lot with the different teachers and colleagues that have crossed my path during those years.


## The project of Module 3: Kids at Brussels Design Museum

After the formation on Fablab, I will participate on the Module 3, where we will work at the ADAM Museum with kids, learning how to introduce the design, the materials, the textures to them. I have identified a lot with this project, because of my passion for the pieces in exhibition at this museum and because kids inspire me to have freedom towards a creation process.

The Brussels Design Museum has a Plastic collection that particullary interests me a lot, from daily to unique items from the twentieth and twenty-first centuries. Some of them have become icons of an era, of a way of life and thinking about life.

We will be 12 students working with the personnel of the Museum and the kids, the best way to promote curiosity and interest on design and its principals to the little ones. Together, we will propose ideas of interaction, such as games, senses development like touch and smell to identify materials, forms, etc…  

Our goal will be to conceive a space of instruction by promoting amusement and an experimental ambiance. Looking forward to learn more during this semester!
