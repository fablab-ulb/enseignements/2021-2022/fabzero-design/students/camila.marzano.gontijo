# Final Project: Kids @ Brussels Design Museum

During the last weeks we have been working on our final project idea. As a part of the Module 3, I participate on the conception of a project that tries to introduce design to kids and to make them feel more interested by the subject. We work with a partnership with the Brussels Design Museum, where this project takes place. 

To start, we have divided the groups. Each group works in a project that will catch the kids attention to the objects exposed in the museum. Our group, composed by Louise, Louison and I, have decided to work with miniatures, as we believe those can speak a lot to kids. They are finally able to touch those objects they have seen on their visit. Besides, it is possible to see details closer, to understand its forms and also, it's an ideal size for a game for example.

Therefore, we decided to search, model and create those miniatures with the 3D printers available on Fablab. This was the point of depart, that was after going to developp into a instructionnal board game.

## Week 1: First ideas

On the first week of work we had many questions regarding our idea... We knew we wanted for sure to work with the miniatures, but what else could we develop in a way that the kids could actually learn something useful. That means, an educational game. We had some ideas in mind, such as work with colors, textures, touch, maybe a timeline... still very general, so we had to do see what would work the best for an educational purpose.

The first idea was to use cards that graphically represented the different characteristics of the objects of the museum that we would have printed out in 3D. The cards would be then, in correspondence with one of the miniatures.

![](images/cartes1.jpg)

During this phase, we still didn't have anything concrete, we were mostly trying out things, but the idea or caracteristics seemed very interesting to us, as the kids would work in their memory, focus and understanding of the visit. We still had to decide how many and which objects we would use for the game in miniature form. It would be impossible to go over them all in a game, so we already had in mind a number between 8 and 12. The children would play the game after the visit. 

## Week 2: Research and choice of objects

During the second week, we created a list of the chosen objects: we tried to select an "iconic" object of each room / podium and therefore of each plastic. We arrived on a number of 8 objects that we have choosen and that we believed to be the most attractive ones for the kids to get to know better. Then we have started to model those objects on Fusion and print them out on the Fablab 3d machines. Some files took to 12 hours to print. We had a few printings that failed but finally when restarted, it worked. 

From those objects we could print out 6 so far. We decided to keep this number as a test for the first try-out of the game with the kids, and then we will see if we add the 2 last objects we have thought of, or if it is already enough for the game time, etc. The objects we chose and printed out are the following ones, next to its location in the museum and its features:

- Cantilever by Verner Panton: Podium "overhang". Features: Technical prowess - resistant - overhang

![](images/cantilever.jpg)

- Dondolo by Cesare Leonardi et Franca Stagi: "Space age" room – GRP (fiberglass). Features: Futuristic/space - Rocking chair - Anti-gravity

![](images/dondolo.jpg)

- Pratone by Pietro Derossi, Giorgio Ceretti et Riccardo Rosso: Podium "radical design" - PUR. Features: Soft - Bigness - Nature look alike

![](images/pratone.JPG)

- Endless flow by Dirk Vander Kooij: Podium "recycled". Features: Recycled - Fridge - 3D printing

![](images/endlessflow.JPG)

- Translation by Alain Gilles: Podium "recycled". Features: Recycled - Composed of lots of small objects - Multicolored 

![](images/translation.jpg)

- Chica by Jonathan de Pas, Donato d'Urbino, Paolo Lomazzi, Giorgio de Curso: First room- ABS. Features: Moldable - Stackable - For kids

![](images/chica.JPG)

Finally a referencing of their characteristics has also been done. We picked up colorful illustrations from an specific site, because we thought those images were more related to a kid then the first pictograms we used. At the end, together with the Fablab team we realized it would be better to keep with the pictograms, to keep it minimalist and simple, but to add maybe a key-word next to the image for an easier understanding for the kids (to see on Week 3). Here are the cards we started to draw:

![](images/cards1.JPG)
 
And here are the rules we have setted up for it:

![](images/regles1.JPG)
 
At this point, we have realized there was something very important missing to our game: a scenario and a base. We needed a support, something that would make it really look like a game and not just like a school activity. We should also improve the game, by adding more to it, maybe creating more rules, more funny things, complicating it a little bit as well... Those were some of the things we have developed on the week 3.

## Week 3: Improving and adding more to the game

This week we have worked hard to get to something that we believe it is much more interesting. We have thought of something anyone could play even if we were not present; a game that had more a face of a game and less of a school activity idea. We thought of a scenario and we made sure to have a base.

We thought it would be nice to use the plan of the Museum as a board, since we were also using the objects in miniature, it made sense to put those together and create a sort of a miniature museum game, where you walk through the museum passing by the miniatures. The game works as a regular boarding game, where there are pawns for each player, dices and the board with a path to follow as you play the game. It also counts with cards, that are divided into 3 cathegories: history, caracteristics and bonus/curiosities. Each one corresponds of a color, and once you stop in the square with one of those colors, you answer a question from the corresponding cathegory. 

Here are the colored cards we have designed and printed out:

![](images/cartes2.jpg)
![](images/cartes3.jpg)

We thought this way the game could be dinamic and that by adding some funny details to it, such as the designers of the miniatures as the players (pawns), the kids would not only have fun, but also learn and discover more of the BDM. 

![](images/designers.jpg)

On the cards we also thought to add 3 options of answer, so the game could flow more easily and the kid would have some indications. Finally, for the price we modeled some 'granulés' and printed them out on 3d, to work as the points to take once you have the good answer, and at the end, once all the 'granulés' are taken, the class can get a common prize! It reinforces the team spirit and it could be a healthier 'competition' for the kids, knowing everyone is contributing.

The 'granulés'/price being printed:

![](images/granules.jpg)

And once they were ready, in the box we made, where we have put the group prize:

![](images/boitegranules1.jpg)

The players, with the head of the designers (the base was cutted out with the Epilog):

![](images/designer.jpg)
 
For the board and the cards, we have designed them ourselves, on computer programs; and then we printed them out with a thicker paper. But for the final game, we would like to improve the quality of printing and have something harder and maybe plastified, to protect from dirt. We also count on making different boxes to put the components of the game and have everything organised in a big box. 

Here follows the scenario and the rules of the game we are going to try this week with the kids:

![](images/regles2.jpg)
 
Here a few pictures of the board of the game, where we put the miniatures how they are actually placed in the museum:

![](images/jeu1.jpg)
![](images/jeu2.jpg)
![](images/jeu3.jpg)
![](images/jeu4.jpg)
 
We are looking forward now to see how it will go and to see the reaction of the kids!

## Testing the game with the Kids: 02/12 on the Fablab 

The game experience with the kids went better than expected. They were very receptive and excited for the game. Once they saw the board they already recognized the plan of the museum and the chairs that they have seen during their visit. We took a couple minutes to explain the rules and to start the game. They chose their 'designer' player and during the path they answered the questions corresponding to the color of each case they stopped by. They were respectful to their friends and they also helped each other out when their collegue didn't know an answer. We could see they really payed attention to the visit they did some weeks ago and that they have a good memory for colors and other visual things: textures, format, etc. We took around 20-25 minutes with each group to finish the game (arrive at the end). Some of the cards were harder than others, so we count to adapt this and put them all on the right level. In general, it works pretty well, they have fun and they learn. So we will mostly focus on the design of the game for the next weeks, such as improving the board, etc. We have played the game with 6 groups of 2 or 3 students each, during the morning and afternoon. It was a fun day for the kids but for us as well!

Some registers of today with the kids:

![](images/jeuenfants1.jpg)
![](images/jeuenfants2.jpg)
![](images/jeuenfants3.jpg)

Here is a little debriefing of what works and what we should improve in the game:

WORKS WELL:

- the game itself worked well: the idea of "being in the museum" in the game (working with space: being located), the board with the boxes/pawns/cards, the number of boxes of the game and cards was ideal;
- they were curious and they remembered the visit and they learned new things;
- they wanted to get to the end, continue the game
- curious by the final price; the badgets worked well
- helping each other worked well, team spirit

TO BE IMPROVED FOR THE NEXT TIME:​

- print objects in the real color as it is in the museum;
- show the object name on the board, next to the 3d;
- more of the green cards (caracteristics);
- give leads in the question (ex: ''when did the BDM open its doors... + knowing that it is a fairly recent museum");
- more distant responses;
- change on the rules: green card 1 point, blue 2, red 3;
- price: 6 small "mini miniatures";
- storage boxes and the design of the board (3 a1 of wood for the board?, improve the cards and the 3d pawns should be more resistant);


## Week 4: doing the changes!

This week we have taken the time to seat and discuss what should we do for our pre jury. We analyzed the list of what we should improve. As time was short and machines were limitred to do everything we actually want to do for the final jury, we have been reasonable on what was more important and urgent to change. So we focused on the game board and on its box.

For the game board we struggled to how we were going to make it foldable without damanging the boad at some point. We also couldn't find the exact measures of 'carton' that we needed, as our game has the specific measurements of the museum. So we came up with a solution that would bring the kids to one more exercise for the game. We decided to cut out the pieces of the board into puzzle parts, which were going to stick to each other to form the museum's plan. So they would also work on their visual and spacial memory. We created big puzzle pieces cutted out with Laserseur. First, we draw the plan with its puzzle divisions, so we could make sure which sizes of 'carton' we needed. Then we passed our file to SVG to be able to cut with the laser machine. We did all the scale adjustments before and luckly everything worked well! We used the 800 speed for the and 40 for the intensity on the machine's settings.

We printed the plan in a better paper this time, and once the pieces of the puzzle / plan were cutted out, we cutted out the plan to glue it into each corresponding piece. Now we had a real board game! 

Check here:

![](images/a8.jpg)
![](images/a10.jpg)
![](images/a6.jpg)

For the cards, we did the changings on some of the questions, giving some tips to the kids to find the good answer. We also added up some of the green cards, as we observed that they were not enough last time. Those were also printed out again in a better and more rigid paper. Green cards added to the game:

![](images/a4.jpg)

For the miniatures, we had to print some of them out again, to have them as the original colors of the objects in the museum, making the game as close as possible to the reality. Unfourtunetly, there is still one chair to be printed out in its real color, the Cantilever. The printing failed so we will have to restart... For the miniatures: we also started to printed out mini versions of our miniatures. Those are supposed to be the prize we will give to each group of kids that play the game! 

![](images/a5.jpg)

For the 'pions' we decided to also print them out in 3D, to compose the game and for a better rigidity. We have modeled them on Fusion 360 and then we did a first try out that worked! Them we put the rest to print on the 3d machine. We haven't been to Fablab since we asked the machine to print (when we left it was in the middle of the process), so we cross our fingers for this thursday to arrive and see them there! 

![](images/a1.jpg)
![](images/a7.jpg)
![](images/a9.jpg)

And last, for this time, we have worked to create a beautiful box to put the game and everything inside! The box is made out of wood, cutted out in the machine. Some of the registers of the process at Louison's house:

![](images/a11.jpg)
![](images/a12.jpg)
![](images/a13.jpg)

For the final jury we still have to improve some things, such as decorate the box, create small boxes or compartiments to keep the small pieces of the game, etc. But we are happy with the progress of the last two weeks and we have the final game idea ready to be built! 

Here are some pictures of the pre jury result:

![](images/prejurychica.jpg)
![](images/prejurypions.jpg)
![](images/prejuryjetons.jpg)
![](images/prejury.jpg)

## From pre Jury to final Jury

Everything went well for the presentations; we spent the day sharing our ideas with the members of the jury and took notes of some things we could still improve for the final jury. We were going on a good direction and the improvements could be seen. Now we have to focus mostly on the design part, continue to improve it to get to a better result!

Some of the things we were told to focus on was: the main board (improve the material, the quality and aesthetics of it), make more rigid pions, make a better selection of the pictogrammes of the green cards (caracteristics) so there is a single line of drawing style. We also had still a couple chairs that weren't yet on its original colors (Dondolo and Cantilever), so for the next presentation we should have them right. We also realized that we needed to improve the box in matters of its weight. It was a bit too heavy because the wood was quite thick, so we might change for a thinner one for the next time. As well as its cover, we would love to make a nice one with the name of the game on it.

We have started then to think about what we were going to do to improve our game. The first weeks of work we have dedicated to really understand how we were going to do. So we started buy first analysing the board, then the small pieces separetly. 

I will explain here what we have done for the final game, item by item:

1) **Le plateau; the board** 

The board was an important part to work, as it is a big part of the game and it has a visual weight. We decided then to start by changing a few details, such as the path of the cases of the game. We changed the squared shapes into round shapes, that gave a better visual in general. We decided to keep the 3 colors: red, blue and green, as it had a positive reaction on the pre jury and a good response from the part of the kids once we played with them, because the colors are a good way of communicating for them. We also changed the font for the same used in the Brussels Design Museum. We made sure to use this exact font on every other item of the game, such as the cards, the rules...To keep an uniformity. We also created a pattern of lines in red and yellow, the colors of the entrance of the BDM. That way we could also differ the space of the plan of the museum with the rest of the space of the board.

After these small changings, we realized it would be interesting to add something more to the board, to make the kids understand the usage of a certain chair, the material used, etc. We added small photos of the chairs or something directly related to it just under the correspondent object. That way, the plan of the museum also had a certain guidance, as the kids could associate more easily where they were located in the museum's plan by seeing the pictures of the reality. We positioned the picture just under the object podium in the plan/board, so it could also look like a little "affiche", as it is in the museum, we see the information just next to the object.

![](images/leplateaufinal.jpg) 
![](images/photopratone.JPG) 

Another step for improving the design of the board, was to create volumes, as if the plan popped out in strategic places; such as the podiums where the objects are in. Therefore, we modeled the podiums to be able to print them out in 3D as well. Each podium took about 8 hours, so it was quite a long process, but worth it, because now the plan had something much more interesting to look at. Also, none of the printings have an issue so they were ready on the first try-outs. 

![](images/dessinpodiums.jpg) 
![](images/podiumsimpression.jpg)
![](images/podiums.jpg)
![](images/podium.jpg)

We decided to keep working on the podiums and create another layer on the top of them: a plexiglass with a "gravure" of the corresponding chair plan view. That way, putting the plexiglass with the gravure on the top of the black podium, the kids and eventual players could easily identify where each chair has to go. 

![](images/podiumsetting.jpg)
![](images/plexis.jpg)

We changed the material of the board for a "bash", something that we could put into one piece only and roll as a "casino board" works. This could be more simple than the idea of the puzzle, that would probrably demand a lot of time for the start of the game for the kids. This material also offers a better quality and nicer aesthetics.

![](images/plateauroll.jpg) 

2) **The pions**

For the pions we had to create another system because the base wans't very stable. Although we liked the idea of putting the heads of the designers, we decided to create something more clear and with the same shapes, that way we could also include the names of the designers and show which of the chairs they have created on the other side. 

![](images/lepionfinal.jpg)

First we modeled the pions in Sketch up anf Fusion to be able to print them out in 3D. The base now is more stable since it is a round surface. 

![](images/pionsmodel.JPG) 
![](images/basepions.jpg)

On the top surface we can put a 3mm cardboard (carton mousse) with the prints of the designers and chairs sticked in. We had to cut them out manually since this material isn't safe to be cutted out on the Laser machine.

The final result looks like real game pions, which was what we were looking for. Here is the result, viewed from front and back:

![](images/pionschaises.jpg)
![](images/pionsdesigners.jpg)

3) **The cards**

It was a hard process for the cards, as it was quite difficult to find a website that had all the pictogrammes we were looking for with the same style. 

Also, what we did, was to select the ones that were a bit too confusing and either remove them or put a double answer when it was the case. 

![](images/cartesenlever.jpg) 

We did a document to separate each chair with their main caracteristics, with the pictogramme that fitted well. Then we did the final selection. We decided to put the same number of cards for each color, therefore we decided that 12 for each one of the categories was a good number, as we got to observe on the time we played with the kids, it was just enough. Now we had 36 cards in total. 

For the cards, we used the same font as the board, to keep an uniformity in the entire game and to make the connection to the existing type of font used in the Museum as well.

4) **The new 3D objects**

We have launched on the 3D printer the two last chairs that weren't yet on the correct color that they are on the museum's exposition. The Dondolo had its first version in blue, so we got the white filament to be able to do it in white, just as it is. 

![](images/dondoloprint.JPG) 

Same for the Cantilever that was in grey, now in red. We had a problem with the first time we tried to print out the Cantilever again, so we had to repeat the process. It was probrably due to a changing of temperature in the room, because we chose the same settings as before. Check here our first test that failed:

![](images/failcantilever.JPG) 

5) **The game rules**

Printed out in a format of a small instructions book, "livret". Now the material choseen was a bit thicker than the last one. It was a special paper used for board games rules as well. Compared to the first version, this one has a cover and information is more clearly divided.

![](images/reglescover.jpg)
![](images/regles.jpg)

6) **The points, the prize and the mini box**

This was a part that was not yet well developped for the pre jury. We already had the idea, the box with a double surface, "double fond". So, for the jury, we decided to model and to print it 3D. It had to be printed into 3 parts, as we can see in the image, it contains the main box and the 2 sliding parts that keeps the prizes and the points of the game. 

For the points, "jétons", we wanted to create something more personalized for the game, that had a direct connection to the Design Museum. So we switched the pink jétons for something more fun. We took a piece of a hard plastic board of about 2mm, something similar to a plexiglass, but with a cream color. 

![](images/plaque.jpg) 

We thought of a regular 10 euro cents coin format to count the points of the game, once someone responds the answer correctly. To make them we used the Epilog. We had to chose a part to cut it out (vector) in the format of coins and then, to personalize them, we used the "gravure". For the gravures we first drawed them in autocad, then we put all together into svg format. The gravures are images of the chairs used in the game.

![](images/dessinjetons.jpg) 

Here are some of the settings used on the Epilog:

![](images/settingsjetons.jpg) 
![](images/jetonslaser.jpg) 
![](images/jetonsepilog.jpg) 

The final result:

![](images/jetons.jpg) 

Finally for the prize, at the end of the game, we modeled some stickers. We thought this would be a fun way for the kids to have a souvnir of the visit and keep it in their notebooks for exemple. For the stickers we preferred to use the pictures of the objects to have the color and texture just how it is in real. Here is how it turned out:

![](images/autocollants.jpg) 

7) **The game box**

For the box of the game we conserved the idea of the wood, since it looked classic and beautiful. It gives a value to the game and this time we chose a darker wood. We also had to change the dimensions, because now the board is supposed to be rolled, different as before. This time, we also created divisions in the box, to be able to separate the different items of the game. We took the measurements to be sure things would fit in. 

For this one, even if it is bigger than the last version, it is lighter, because the wood board was less thick. 

We also added something to hold the box, making it easy to transport anywhere, just like a suitcase. To open it, we just have to slide the top board, like a wardrobe system.

Here are some pictures of the process and the final result:

![](images/boitebois.JPG) 
![](images/boiteboisdivision.JPG) 
![](images/boiteferme.jpg) 
![](images/boiteouverte.jpg) 

**THE FINAL RESULT OF KIDS AT BDM: MINIMUSEUM GAME**

![](images/jeufinalbdm.jpg) 
